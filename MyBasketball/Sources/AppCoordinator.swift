//
//  AppCoordinator.swift
//  MyBasketball
//
//  Created by Pedro Veloso on 29/08/20.
//  Copyright © 2020 Pedro Veloso. All rights reserved.
//

import UIKit

final class AppCoordinator {
    enum State {
        case home

        var viewController: UIViewController {
            return TrainingCollectionBuilder().build()
        }
    }

    static let shared = AppCoordinator()
    private var window = UIWindow()

    private init() {}

    func build(window: UIWindow) -> AppCoordinator {
        self.window = window
        return self
    }

    func start(state: State = .home) {
        window.rootViewController = setupNavigation(for: state.viewController)
        window.makeKeyAndVisible()
    }
}

private extension AppCoordinator {
    func setupNavigation(for root: UIViewController) -> UINavigationController {
        let navigation = UINavigationController(rootViewController: root)
        navigation.navigationBar.barTintColor = .darkBlue
        navigation.navigationBar.shadowImage = nil
        navigation.navigationBar.isTranslucent = false
        return navigation
    }
}
