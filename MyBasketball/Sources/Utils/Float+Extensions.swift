//
//  Float+Extensions.swift
//  MyBasketball
//
//  Created by Pedro Veloso on 27/08/20.
//  Copyright © 2020 Pedro Veloso. All rights reserved.
//

import Foundation

extension Float {
    var toString: String {
        return String(format: "%.2f", self)
    }
}
