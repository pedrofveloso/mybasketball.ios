//
//  Date+Extensions.swift
//  MyBasketball
//
//  Created by Pedro Veloso on 27/08/20.
//  Copyright © 2020 Pedro Veloso. All rights reserved.
//

import Foundation

extension NSPredicate {
    func sameDateMatches(for date: Date) -> NSPredicate {
        var calendar = Calendar.current
        calendar.timeZone = NSTimeZone.local

        let dateFrom = calendar.startOfDay(for: Date())
        guard let dateTo = calendar.date(byAdding: .day, value: 1, to: dateFrom) else { return self }

        let fromPredicate = NSPredicate(format: "%@ >= %@", date as NSDate, dateFrom as NSDate)
        let toPredicate = NSPredicate(format: "%@ < %@", date as NSDate, dateTo as NSDate)

        return NSCompoundPredicate(andPredicateWithSubpredicates: [fromPredicate, toPredicate])
    }
}
