//
//  UIImage+Extensions.swift
//  MyBasketball
//
//  Created by Pedro Veloso on 20/08/20.
//  Copyright © 2020 Pedro Veloso. All rights reserved.
//

import UIKit

extension UIImage {
    func scaleTo(_ newSize: CGSize) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0)
        self.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        let newImage: UIImage? = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage ?? self
    }
}
