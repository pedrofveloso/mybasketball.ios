//
//  ViewCode.swift
//  MyBasketball
//
//  Created by Pedro Veloso on 20/08/20.
//  Copyright © 2020 Pedro Veloso. All rights reserved.
//

import Foundation

protocol ViewCodable {
    func buildViewCode()
    func buildStack()
    func buildConstraints()
    func additionalSetup()
}

extension ViewCodable {
    func buildViewCode() {
        buildStack()
        buildConstraints()
        additionalSetup()
    }
}
