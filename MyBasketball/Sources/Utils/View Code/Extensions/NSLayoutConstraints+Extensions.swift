//
//  NSLayoutConstraints+Extensions.swift
//  MyBasketball
//
//  Created by Pedro Veloso on 20/08/20.
//  Copyright © 2020 Pedro Veloso. All rights reserved.
//

import UIKit

extension UIView {
    func topConstraint(parent: UIView,
                       constant: CGFloat = 0,
                       relatedBy relation: NSLayoutConstraint.Relation = .equal,
                       multiplier: CGFloat = 1) {
        setupVerticalConstraint(anchor: self.topAnchor,
                                parentAnchor: parent.safeAreaLayoutGuide.topAnchor,
                                constant: constant,
                                relation: relation)
    }

    func leftConstraint(parent: UIView,
                        constant: CGFloat = 0,
                        relatedBy relation: NSLayoutConstraint.Relation = .equal,
                        multiplier: CGFloat = 1) {
        setupHorizontalConstraint(anchor: self.leadingAnchor,
                                  parentAnchor: parent.safeAreaLayoutGuide.leadingAnchor,
                                  constant: constant,
                                  relation: relation)
    }

    func rightConstraint(parent: UIView,
                         constant: CGFloat = 0,
                         relatedBy relation: NSLayoutConstraint.Relation = .equal,
                         multiplier: CGFloat = 1) {
        setupHorizontalConstraint(anchor: self.trailingAnchor,
                                  parentAnchor: parent.safeAreaLayoutGuide.trailingAnchor,
                                  constant: -constant,
                                  relation: relation)
    }

    func bottomConstraint(parent: UIView,
                          constant: CGFloat = 0,
                          relatedBy relation: NSLayoutConstraint.Relation = .equal,
                          multiplier: CGFloat = 1) {
        setupVerticalConstraint(anchor: self.bottomAnchor,
                                parentAnchor: parent.safeAreaLayoutGuide.bottomAnchor,
                                constant: -constant,
                                relation: relation)
    }

    func verticalConstraint(parent: UIView,
                            constant: CGFloat = 0,
                            relatedBy relation: NSLayoutConstraint.Relation = .equal,
                            multiplier: CGFloat = 1) {
        self.topConstraint(parent: parent, constant: constant)
        self.bottomConstraint(parent: parent, constant: constant)
    }

    func horizontalConstraint(parent: UIView,
                              constant: CGFloat = 0,
                              relatedBy relation: NSLayoutConstraint.Relation = .equal,
                              multiplier: CGFloat = 1) {
        self.leftConstraint(parent: parent, constant: constant)
        self.rightConstraint(parent: parent, constant: constant)
    }

    func overConstraint(parent: UIView,
                        constant: CGFloat = 0,
                        relatedBy relation: NSLayoutConstraint.Relation = .equal,
                        multiplier: CGFloat = 1) {
        self.setupVerticalConstraint(anchor: self.topAnchor,
                                     parentAnchor: parent.safeAreaLayoutGuide.bottomAnchor,
                                     constant: constant,
                                     relation: relation)
    }

    func heightConstraint(constant: CGFloat,
                          relatedBy relation: NSLayoutConstraint.Relation = .equal) {
        self.setupDimensionConstraint(anchor: self.heightAnchor,
                                      constant: constant,
                                      relatedBy: relation)
    }

    func widthConstraint(constant: CGFloat,
                         relatedBy relation: NSLayoutConstraint.Relation = .equal) {
        self.setupDimensionConstraint(anchor: self.widthAnchor,
                                      constant: constant,
                                      relatedBy: relation)
    }

    func squareConstraint(constant: CGFloat,
                          relatedBy relation: NSLayoutConstraint.Relation = .equal) {
        self.heightConstraint(constant: constant)
        self.widthConstraint(constant: constant)
    }

    func asideConstraint(parent: UIView,
                         constant: CGFloat = 0,
                         relatedBy relation: NSLayoutConstraint.Relation = .equal,
                         multiplier: CGFloat = 1) {
        self.setupHorizontalConstraint(anchor: self.leadingAnchor,
                                       parentAnchor: parent.safeAreaLayoutGuide.trailingAnchor,
                                       constant: constant,
                                       relation: relation)
    }

    func centerXConstraint(parent: UIView,
                           constant: CGFloat = 0,
                           relatedBy relation: NSLayoutConstraint.Relation = .equal,
                           multiplier: CGFloat = 1) {
        self.setupHorizontalConstraint(anchor: self.centerXAnchor,
                                       parentAnchor: parent.safeAreaLayoutGuide.centerXAnchor,
                                       constant: constant,
                                       relation: relation)
    }

    func centerYConstraint(parent: UIView,
                           constant: CGFloat = 0,
                           relatedBy relation: NSLayoutConstraint.Relation = .equal,
                           multiplier: CGFloat = 1) {
        self.setupVerticalConstraint(anchor: self.centerYAnchor,
                                     parentAnchor: parent.safeAreaLayoutGuide.centerYAnchor,
                                     constant: constant,
                                     relation: relation)
    }
}

private extension UIView {
    func setupHorizontalConstraint(anchor: NSLayoutXAxisAnchor,
                                   parentAnchor: NSLayoutXAxisAnchor,
                                   constant: CGFloat,
                                   relation: NSLayoutConstraint.Relation) {
        let constraint: NSLayoutConstraint
        switch relation {
        case .equal:
            constraint = anchor.constraint(equalTo: parentAnchor, constant: constant)
        case .lessThanOrEqual:
            constraint = anchor.constraint(lessThanOrEqualTo: parentAnchor, constant: constant)
        case .greaterThanOrEqual:
            constraint = anchor.constraint(greaterThanOrEqualTo: parentAnchor, constant: constant)
        @unknown default:
            return
        }
        constraint.isActive = true
    }

    func setupVerticalConstraint(anchor: NSLayoutYAxisAnchor,
                                 parentAnchor: NSLayoutYAxisAnchor,
                                 constant: CGFloat,
                                 relation: NSLayoutConstraint.Relation) {
        let constraint: NSLayoutConstraint
        switch relation {
        case .equal:
            constraint = anchor.constraint(equalTo: parentAnchor, constant: constant)
        case .lessThanOrEqual:
            constraint = anchor.constraint(lessThanOrEqualTo: parentAnchor, constant: constant)
        case .greaterThanOrEqual:
            constraint = anchor.constraint(greaterThanOrEqualTo: parentAnchor, constant: constant)
        @unknown default:
            return
        }
        constraint.isActive = true
    }

    func setupDimensionalConstraints(anchor: NSLayoutDimension,
                                     parentAnchor: NSLayoutDimension,
                                     constant: CGFloat,
                                     relation: NSLayoutConstraint.Relation) {
        let constraint: NSLayoutConstraint
        switch relation {
        case .equal:
            constraint = anchor.constraint(equalTo: parentAnchor, constant: constant)
        case .lessThanOrEqual:
            constraint = anchor.constraint(lessThanOrEqualTo: parentAnchor, constant: constant)
        case .greaterThanOrEqual:
            constraint = anchor.constraint(greaterThanOrEqualTo: parentAnchor, constant: constant)
        @unknown default:
            return
        }
        constraint.isActive = true
    }

    func setupDimensionConstraint(anchor: NSLayoutDimension,
                                  constant: CGFloat,
                                  relatedBy relation: NSLayoutConstraint.Relation) {
        let constraint: NSLayoutConstraint
        switch relation {
        case .equal:
            constraint = anchor.constraint(equalToConstant: constant)
        case .lessThanOrEqual:
            constraint = anchor.constraint(lessThanOrEqualToConstant: constant)
        case .greaterThanOrEqual:
            constraint = anchor.constraint(greaterThanOrEqualToConstant: constant)
        @unknown default:
            return
        }
        constraint.isActive = true
    }
}
