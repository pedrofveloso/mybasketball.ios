//
//  UIView+Extensions.swift
//  MyBasketball
//
//  Created by Pedro Veloso on 21/08/20.
//  Copyright © 2020 Pedro Veloso. All rights reserved.
//

import UIKit

extension UIView {
    func setTranslatesAutoresizingMaskIntoConstraints(for views: [UIView], with value: Bool = false) {
        views.forEach { view in
            view.translatesAutoresizingMaskIntoConstraints = value
        }
    }
}
