//
//  Label.swift
//  MyBasketball
//
//  Created by Pedro Veloso on 21/08/20.
//  Copyright © 2020 Pedro Veloso. All rights reserved.
//

import UIKit

final class Label: UILabel {
    enum FontType {
        case normal, bold

        var name: String {
            switch self {
            case .normal: return "Avenir"
            case .bold: return "Avenir-Heavy"
            }
        }
    }

    init(text: String = "",
         size: CGFloat = 18.0,
         color: UIColor = .black,
         type: FontType = .normal,
         frame: CGRect = .zero) {
        super.init(frame: frame)
        self.text = text
        self.textColor = color
        self.translatesAutoresizingMaskIntoConstraints = false
        setupFont(size: size, type: type)
    }

    required init?(coder: NSCoder) { return nil }
}

private extension Label {
    func setupFont(size: CGFloat, type: FontType) {
        self.font = UIFont(name: type.name, size: size)
    }
}
