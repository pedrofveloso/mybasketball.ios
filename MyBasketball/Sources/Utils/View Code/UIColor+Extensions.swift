//
//  UIColor+Extensions.swift
//  MyBasketball
//
//  Created by Pedro Veloso on 29/08/20.
//  Copyright © 2020 Pedro Veloso. All rights reserved.
//

import UIKit

extension UIColor {
    class var darkBlue: UIColor {
        return UIColor(red: 0.02, green: 0.42, blue: 0.67, alpha: 1)
    }
}
