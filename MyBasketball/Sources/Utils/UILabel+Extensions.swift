//
//  UILabel+Extensions.swift
//  MyBasketball
//
//  Created by Pedro Veloso on 27/08/20.
//  Copyright © 2020 Pedro Veloso. All rights reserved.
//

import UIKit

extension UILabel {
    func addColorAttributes(attrTexts: String..., attrColor: UIColor? = nil) {
        attrTexts.forEach { text in
            addAttribute(attrText: text, attrColor: attrColor)
        }
    }

    func addFontWeightAttributes(attrTexts: String..., attrFontWeight: UIFont.Weight? = nil) {
        attrTexts.forEach { text in
            addAttribute(attrText: text, attrFontWeight: attrFontWeight)
        }
    }
}

private extension UILabel {
    func addAttribute(attrText: String,
                      attrColor: UIColor? = nil,
                      attrFontWeight: UIFont.Weight? = nil) {
        guard let text = self.text else { return }
        let range = (text as NSString).range(of: attrText)

        let attributedString = NSMutableAttributedString(attributedString: self.attributedText ??
            NSAttributedString(string: text))

        let color: UIColor = attrColor ?? self.textColor
        attributedString.addAttribute(NSAttributedString.Key.foregroundColor,
                                      value: color,
                                      range: range)
        self.attributedText = attributedString
    }
}
