//
//  DataManager.swift
//  MyBasketball
//
//  Created by Pedro Veloso on 26/08/20.
//  Copyright © 2020 Pedro Veloso. All rights reserved.
//
import CoreData
import Foundation

final class DataManager {
    static let shared: DataManager = DataManager()

    lazy var persistentContainer: NSPersistentContainer = {
        if #available(iOS 13.0, *) {
            let container = NSPersistentCloudKitContainer(name: "MyBasketball")
            container.loadPersistentStores(completionHandler: { (_, error) in
                if let error = error as NSError? {
                    /*
                     Typical reasons for an error here include:
                     * The parent directory does not exist, cannot be created, or disallows writing.
                     * The persistent store is not accessible,
                     due to permissions or data protection when the device is locked.
                     * The device is out of space.
                     * The store could not be migrated to the current model version.
                     Check the error message to determine what the actual problem was.
                     */
                    fatalError("Unresolved error \(error), \(error.userInfo)")
                }
            })
            return container
        } else {
            let container = NSPersistentContainer(name: "MyBasketball")
            container.loadPersistentStores(completionHandler: { (_, error) in
                if let error = error as NSError? {
                    fatalError("Unresolved error \(error), \(error.userInfo)")
                }
            })
            return container
        }
    }()

    private init() {}

    // MARK: - Core Data Saving support
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
}
