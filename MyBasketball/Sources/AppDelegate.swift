//
//  AppDelegate.swift
//  MyBasketball
//
//  Created by Pedro Veloso on 20/08/20.
//  Copyright © 2020 Pedro Veloso. All rights reserved.
//

import UIKit
import CoreData

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?

    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        window = UIWindow(frame: UIScreen.main.bounds)
        AppCoordinator.shared.build(window: window!).start()
        return true
    }
}
