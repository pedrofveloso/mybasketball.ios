//
//  TrainingCollectionPresenter.swift
//  MyBasketball
//
//  Created by Pedro Veloso on 29/08/20.
//  Copyright © 2020 Pedro Veloso. All rights reserved.
//

import Foundation

protocol TrainingCollectionPresentable: AnyObject {
    
}

final class TrainingCollectionPresenter {
    weak var controller: TrainingCollectionViewControllable?
}

extension TrainingCollectionPresenter: TrainingCollectionPresentable {

}
