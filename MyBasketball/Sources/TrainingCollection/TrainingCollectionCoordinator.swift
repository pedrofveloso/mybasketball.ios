//
//  TrainingCollectionCoordinator.swift
//  MyBasketball
//
//  Created by Pedro Veloso on 29/08/20.
//  Copyright © 2020 Pedro Veloso. All rights reserved.
//

import UIKit

protocol TrainingCollectionCoordinatable: AnyObject {
    func set(navigation: UINavigationController?)
}

final class TrainingCollectionCoordinator {
    private var navigation: UINavigationController?
}

extension TrainingCollectionCoordinator: TrainingCollectionCoordinatable {
    func set(navigation: UINavigationController?) {
        self.navigation = navigation
    }
}
