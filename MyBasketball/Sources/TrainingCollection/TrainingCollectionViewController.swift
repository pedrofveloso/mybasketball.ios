//
//  TrainingCollectionViewController.swift
//  MyBasketball
//
//  Created by Pedro Veloso on 29/08/20.
//  Copyright © 2020 Pedro Veloso. All rights reserved.
//

import UIKit

protocol TrainingCollectionViewControllable: AnyObject {

}

final class TrainingCollectionViewController: UIViewController {
    private let interactor: TrainingCollectionInteractable
    private let coordinator: TrainingCollectionCoordinatable

    init(interactor: TrainingCollectionInteractable,
         coordinator: TrainingCollectionCoordinatable = TrainingCollectionCoordinator()) {
        self.interactor = interactor
        self.coordinator = coordinator
        super.init(nibName: nil, bundle: nil)

        self.coordinator.set(navigation: navigationController)
    }

    required init?(coder: NSCoder) { return nil }

    override func viewDidLoad() {
        self.view.backgroundColor = .white
    }
}

extension TrainingCollectionViewController: TrainingCollectionViewControllable {}
