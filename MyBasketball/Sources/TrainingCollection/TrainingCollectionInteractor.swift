//
//  TrainingCollectionInteractor.swift
//  MyBasketball
//
//  Created by Pedro Veloso on 29/08/20.
//  Copyright © 2020 Pedro Veloso. All rights reserved.
//

import Foundation

protocol TrainingCollectionInteractable: AnyObject {
    
}

final class TrainingCollectionInteractor {
    private let presenter: TrainingCollectionPresentable

    init(presenter: TrainingCollectionPresentable) {
        self.presenter = presenter
    }
}

extension TrainingCollectionInteractor: TrainingCollectionInteractable {
    
}
