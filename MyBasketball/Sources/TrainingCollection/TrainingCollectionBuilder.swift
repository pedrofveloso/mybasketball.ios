//
//  TrainingCollectionBuilder.swift
//  MyBasketball
//
//  Created by Pedro Veloso on 29/08/20.
//  Copyright © 2020 Pedro Veloso. All rights reserved.
//

import UIKit

struct TrainingCollectionBuilder {
    func build() -> TrainingCollectionViewController {
        let presenter = TrainingCollectionPresenter()
        let interactor = TrainingCollectionInteractor(presenter: presenter)
        let viewController = TrainingCollectionViewController(interactor: interactor)
        presenter.controller = viewController
        return viewController
    }
}
